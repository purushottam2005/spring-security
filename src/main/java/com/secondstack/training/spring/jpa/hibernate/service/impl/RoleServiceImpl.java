package com.secondstack.training.spring.jpa.hibernate.service.impl;

import com.secondstack.training.spring.jpa.hibernate.domain.security.Role;
import com.secondstack.training.spring.jpa.hibernate.service.RoleService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Role: LATIEF-NEW
 * Date: 4/22/13
 * Time: 9:26 AM
 * To change this template use File | Settings | File Templates.
 */
@Service
@Transactional
public class RoleServiceImpl implements RoleService {

    @PersistenceContext
    protected EntityManager entityManager;

    @Override
    public void save(Role role) {
        entityManager.persist(role);
    }

    @Override
    public void delete(Role role){
        entityManager.remove(role);
    }

    @Override
    public void delete(String authority){
        entityManager.remove(findByAuthority(authority));
    }

    @Override
    public List<Role> findAll(){
        List<Role> results = entityManager.createQuery("SELECT s from Role s", Role.class).getResultList();
        return results;
    }
    @Override
    public Role findByAuthority(String authority){
        return entityManager.find(Role.class, authority);
    }
}
