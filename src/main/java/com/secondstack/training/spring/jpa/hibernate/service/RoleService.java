package com.secondstack.training.spring.jpa.hibernate.service;

import com.secondstack.training.spring.jpa.hibernate.domain.security.Role;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Role: LATIEF-NEW
 * Date: 4/22/13
 * Time: 9:25 AM
 * To change this template use File | Settings | File Templates.
 */
public interface RoleService {
    void save(Role role);
    void delete(Role role);
    void delete(String authority);
    List<Role> findAll();
    Role findByAuthority(String authority);
}
