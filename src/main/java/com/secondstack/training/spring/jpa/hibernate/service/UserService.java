package com.secondstack.training.spring.jpa.hibernate.service;

import com.secondstack.training.spring.jpa.hibernate.domain.security.Role;
import com.secondstack.training.spring.jpa.hibernate.domain.security.User;
import com.secondstack.training.spring.jpa.hibernate.dto.SimpleUser;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: LATIEF-NEW
 * Date: 4/22/13
 * Time: 9:25 AM
 * To change this template use File | Settings | File Templates.
 */
public interface UserService {
    void save(User user);
    User save(SimpleUser simpleUser);
    User update(Integer id, User user);
    User update(Integer id, SimpleUser simpleUser);
    void delete(User user);
    void delete(Integer id);
    List<User> findAll();
    List<User> findByRole(Role role);
    User findById(Integer id);
    User findByUsername(String username);
}
