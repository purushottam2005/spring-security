package com.secondstack.training.spring.jpa.hibernate.controller.api;

import com.secondstack.training.spring.jpa.hibernate.domain.Lecture;
import com.secondstack.training.spring.jpa.hibernate.domain.Subject;
import com.secondstack.training.spring.jpa.hibernate.domain.Teacher;
import com.secondstack.training.spring.jpa.hibernate.service.SubjectService;
import com.secondstack.training.spring.jpa.hibernate.service.TeacherService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: LATIEF-NEW
 * Date: 4/19/13
 * Time: 9:08 PM
 * To change this template use File | Settings | File Templates.
 */
@Controller
//@RequestMapping(value = "subject", headers = {"Accept=application/json"})
@RequestMapping(value = "/api/subject")
public class SubjectApiController {

    protected static Logger logger = Logger.getLogger("controller");

    @Autowired
    private SubjectService subjectService;

    @Autowired
    private TeacherService teacherService;

    @RequestMapping(method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    public void create(@RequestBody Subject subject) {
        logger.debug("Received rest request to create subject");
        subjectService.save(subject);
    }

    @RequestMapping(params = {"id"}, method = RequestMethod.PUT)
    @ResponseStatus(HttpStatus.OK)
    public void update(@RequestParam("id") Integer id, @RequestBody Subject subject) {
        logger.debug("Received rest request to update subject");
        subjectService.update(id, subject);
    }

    @RequestMapping(method = RequestMethod.GET)
    @ResponseBody
    public List<Subject> findAll(ModelMap modelMap) {
        logger.debug("Received rest request to get list subject");
        List<Subject> subjectList = subjectService.findAll();
        return subjectList;
    }

    @RequestMapping(params = {"id"}, method = RequestMethod.GET)
    @ResponseBody
    public Subject findById(@RequestParam("id") Integer id) {
        logger.debug("Received rest request to get data subject");
        Subject subject = subjectService.findById(id);
        return subject;
    }

    @RequestMapping(params = {"teacher.id"}, method = RequestMethod.GET)
    @ResponseBody
    public List<Subject> findByTeacher(@RequestParam("teacher.id") Integer teacherId) {
        logger.debug("Received rest request to get list subject");
        Teacher teacher = teacherService.findById(teacherId);
        List<Subject> subjectList = subjectService.findByTeacher(teacher);
        return subjectList;
    }

    @RequestMapping(params = {"id"}, method = RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.OK)
    public void delete(@RequestParam("id") Integer id) {
        logger.debug("Received rest request to delete subject");
        subjectService.delete(id);
    }

}
