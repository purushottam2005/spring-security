package com.secondstack.training.spring.jpa.hibernate.controller.api;

import com.secondstack.training.spring.jpa.hibernate.domain.Lecture;
import com.secondstack.training.spring.jpa.hibernate.domain.Teacher;
import com.secondstack.training.spring.jpa.hibernate.service.LectureService;
import com.secondstack.training.spring.jpa.hibernate.service.TeacherService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: LATIEF-NEW
 * Date: 4/19/13
 * Time: 9:08 PM
 * To change this template use File | Settings | File Templates.
 */
@Controller
@RequestMapping(value = "lecture", headers = {"Accept=application/json"})
//@RequestMapping(value = "/api/lecture")
public class LectureApiController {

    protected static Logger logger = Logger.getLogger("controller");

    @Autowired
    private LectureService lectureService;

    @Autowired
    private TeacherService teacherService;

    @RequestMapping(method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    public void create(@RequestBody Lecture lecture){
        logger.debug("Received rest request to create lecture");
        lectureService.save(lecture);
    }

    @RequestMapping(params = {"id"}, method = RequestMethod.PUT)
    @ResponseStatus(HttpStatus.OK)
    public void update(@RequestParam("id")Integer id, @RequestBody Lecture lecture){
        logger.debug("Received rest request to update lecture");
        lectureService.update(id, lecture);
    }

    @RequestMapping(method = RequestMethod.GET)
    @ResponseBody
    public List<Lecture> findAll(){
        logger.debug("Received rest request to get list lecture");
        List<Lecture> lectureList = lectureService.findAll();
        return lectureList;
    }

    @RequestMapping(params = {"teacher.id"}, method = RequestMethod.GET)
    @ResponseBody
    public List<Lecture> findByTeacher(@RequestParam("teacher.id")Integer teacherId){
        logger.debug("Received rest request to get list lecture");
        Teacher teacher = teacherService.findById(teacherId);
        List<Lecture> lectureList = lectureService.findByTeacher(teacher);
        return lectureList;
    }

    @RequestMapping(value = "/simple1", params = {"teacher.id"}, method = RequestMethod.GET)
    @ResponseBody
    public List findSimpleByTeacher1(@RequestParam("teacher.id") Integer teacherId){
        logger.debug("Received rest request to get list lecture");
        Teacher teacher = teacherService.findById(teacherId);
        List lectureList = lectureService.findSimpleByTeacher1(teacher);
        return lectureList;
    }

    @RequestMapping(value = "/simple2", params = {"teacher.id"}, method = RequestMethod.GET)
    @ResponseBody
    public List findSimpleByTeacher2(@RequestParam("teacher.id")Integer teacherId){
        logger.debug("Received rest request to get list lecture");
        Teacher teacher = teacherService.findById(teacherId);
        List lectureList = lectureService.findSimpleByTeacher2(teacher);
        return lectureList;
    }

    @RequestMapping(value = "/simple3", params = {"teacher.id"}, method = RequestMethod.GET)
    @ResponseBody
    public List findSimpleByTeacher3(@RequestParam("teacher.id")Integer teacherId){
        logger.debug("Received rest request to get list lecture");
        Teacher teacher = teacherService.findById(teacherId);
        List lectureList = lectureService.findSimpleByTeacher3(teacher);
        return lectureList;
    }

    @RequestMapping(params = {"id"}, method = RequestMethod.GET)
    @ResponseBody
    public Lecture findById(@RequestParam("id")Integer id){
        logger.debug("Received rest request to get data lecture");
        Lecture lecture = lectureService.findById(id);
        return lecture;
    }

    @RequestMapping(params = {"id"}, method = RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.OK)
    public void delete(@RequestParam("id")Integer id){
        logger.debug("Received rest request to delete lecture");
        lectureService.delete(id);
    }

}
