package com.secondstack.training.spring.jpa.hibernate.dto;

import com.secondstack.training.spring.jpa.hibernate.domain.enumeration.Semester;

/**
 * Created with IntelliJ IDEA.
 * User: LATIEF-NEW
 * Date: 4/27/13
 * Time: 12:39 PM
 * To change this template use File | Settings | File Templates.
 */
public class SimpleLecture {
    private Integer id;
    private Semester semester;
    private Integer year;
    private String subjectName;
    private String teacherFirstName;

    public SimpleLecture() {
    }

    public SimpleLecture(Integer id, Semester semester, Integer year, String subjectName, String teacherFirstName) {
        this.id = id;
        this.year = year;
        this.semester = semester;
        this.subjectName = subjectName;
        this.teacherFirstName = teacherFirstName;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public Semester getSemester() {
        return semester;
    }

    public void setSemester(Semester semester) {
        this.semester = semester;
    }

    public String getSubjectName() {
        return subjectName;
    }

    public void setSubjectName(String subjectName) {
        this.subjectName = subjectName;
    }

    public String getTeacherFirstName() {
        return teacherFirstName;
    }

    public void setTeacherFirstName(String teacherFirstName) {
        this.teacherFirstName = teacherFirstName;
    }
}
