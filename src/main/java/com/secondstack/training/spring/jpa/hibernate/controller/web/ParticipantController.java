package com.secondstack.training.spring.jpa.hibernate.controller.web;

import com.secondstack.training.spring.jpa.hibernate.domain.Lecture;
import com.secondstack.training.spring.jpa.hibernate.domain.Participant;
import com.secondstack.training.spring.jpa.hibernate.service.*;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Created with IntelliJ IDEA.
 * User: LATIEF-NEW
 * Date: 4/19/13
 * Time: 9:08 PM
 * To change this template use File | Settings | File Templates.
 */
@Controller
@RequestMapping("participant")
public class ParticipantController {

    protected static Logger logger = Logger.getLogger("controller");

    @Autowired
    private ParticipantService participantService;

    @Autowired
    private LectureService lectureService;

    @Autowired
    private SubjectService subjectService;

    @Autowired
    private TeacherService teacherService;

    @Autowired
    private StudentService studentService;

    @RequestMapping(value = "/form", method = RequestMethod.GET)
    public String form(ModelMap modelMap) {
        logger.debug("Received request to get form participant");

        Participant participant = new Participant();
        modelMap.addAttribute("participant", participant);
        modelMap.addAttribute("participantUrl", "/participant");
        modelMap.addAttribute("menuParticipantClass", "active");

        modelMap.addAttribute("lectureList", lectureService.findAll());

        return "participant-form-tiles";
    }

    @RequestMapping(method = RequestMethod.POST)
    public String create(@ModelAttribute("lectureList") Lecture[] lectures, ModelMap modelMap){
        logger.debug("Received request to create participant");

//        participantService.save(participant);
//        modelMap.addAttribute("participant", participant);
//        modelMap.addAttribute("menuParticipantClass", "active");
        System.out.println(lectures);
        return "/";
    }
//
//    @RequestMapping(value = "form", params = "id", method = RequestMethod.GET)
//    public String form(@RequestParam("id")Integer id, ModelMap modelMap){
//        logger.debug("Received request to get form participant");
//
//        Participant participant = participantService.findByAuthority(id);
//        modelMap.addAttribute("participant", participant);
//        modelMap.addAttribute("participantUrl", "/participant?id=" + id);
//        modelMap.addAttribute("menuParticipantClass", "active");
//
//        return "participant-form-tiles";
//    }
//
//    @RequestMapping(params = {"id"}, method = RequestMethod.POST)
//    public String update(@RequestParam("id")Integer id, @ModelAttribute("participant") Participant participant, ModelMap modelMap){
//        logger.debug("Received request to update participant");
//
//        participantService.update(id, participant);
//        modelMap.addAttribute("participant", participant);
//        modelMap.addAttribute("menuParticipantClass", "active");
//
//        return "participant-data-tiles";
//    }
//
//    @RequestMapping(method = RequestMethod.GET)
//    public String findAll(ModelMap modelMap) throws SQLException{
//        logger.debug("Received request to get list participant");
//
//        List<Participant> participantList = participantService.findAll();
//        modelMap.addAttribute("participantList", participantList);
//        modelMap.addAttribute("menuParticipantClass", "active");
//
//        return "participant-list-tiles";
//    }
//
//    @RequestMapping(params = {"id"}, method = RequestMethod.GET)
//    public String findByAuthority(@RequestParam("id")Integer id, ModelMap modelMap) throws SQLException{
//        logger.debug("Received request to get data participant");
//
//        Participant participant = participantService.findByAuthority(id);
//        modelMap.addAttribute("participant", participant);
//        modelMap.addAttribute("menuParticipantClass", "active");
//
//        return "participant-data-tiles";
//    }
//
//    @RequestMapping(value = "delete", params = {"id"}, method = RequestMethod.GET)
//    public String delete(@RequestParam("id")Integer id, ModelMap modelMap) throws SQLException{
//        logger.debug("Received request to delete participant");
//
//        participantService.delete(id);
//
//        return "redirect:/participant";
//    }

}
