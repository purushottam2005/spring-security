package com.secondstack.training.spring.jpa.hibernate.controller.web;

import com.secondstack.training.spring.jpa.hibernate.domain.security.User;
import com.secondstack.training.spring.jpa.hibernate.dto.SimpleUser;
import com.secondstack.training.spring.jpa.hibernate.service.RoleService;
import com.secondstack.training.spring.jpa.hibernate.service.UserService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.encoding.PasswordEncoder;
import org.springframework.security.crypto.codec.Hex;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: LATIEF-NEW
 * Date: 4/19/13
 * Time: 9:08 PM
 * To change this template use File | Settings | File Templates.
 */
@Controller
@RequestMapping("user")
public class UserController {

    protected static Logger logger = Logger.getLogger("controller");

    @Autowired
    private UserService userService;

    @Autowired
    private RoleService roleService;//++++

    @Autowired
    private PasswordEncoder passwordEncoder;

    @RequestMapping(value = "/form", method = RequestMethod.GET)
    public String form(ModelMap modelMap) {
        logger.debug("Received request to get form user");

        SimpleUser simpleUser = new SimpleUser();//++++
        modelMap.addAttribute("user", simpleUser);//++++
        modelMap.addAttribute("userUrl", "/user");
        modelMap.addAttribute("menuUserClass", "active");

        modelMap.addAttribute("roleList", roleService.findAll());//++++

        return "user-form-tiles";
    }

    @RequestMapping(method = RequestMethod.POST)
    public String create(@ModelAttribute("user") SimpleUser simpleUser, ModelMap modelMap){//++++
        logger.debug("Received request to create user");

        String salt = new String(Hex.encode(simpleUser.getUsername().getBytes()));  //++++
        simpleUser.setSalt(salt);                       //++++
        simpleUser.setPassword(passwordEncoder.encodePassword(simpleUser.getPassword(), salt));//++++
        simpleUser.setEnabled(true);//++++
        User user = userService.save(simpleUser);     //++++
        modelMap.addAttribute("user", user);
        modelMap.addAttribute("menuUserClass", "active");

        return "user-data-tiles";
    }

    @RequestMapping(value = "/form", params = "id", method = RequestMethod.GET)
    public String form(@RequestParam("id")Integer id, ModelMap modelMap){
        logger.debug("Received request to get form user");

        User user = userService.findById(id);         //++++
        SimpleUser simpleUser = new SimpleUser();     //++++
        simpleUser.cloneValueFrom(user);         //++++
        modelMap.addAttribute("user", simpleUser);       //++++
        modelMap.addAttribute("userUrl", "/user?id=" + id);
        modelMap.addAttribute("menuUserClass", "active");

        modelMap.addAttribute("roleList", roleService.findAll());//++++

        return "user-form-tiles";
    }

    @RequestMapping(params = {"id"}, method = RequestMethod.POST)
    public String update(@RequestParam("id")Integer id, @ModelAttribute("user") SimpleUser simpleUser, ModelMap modelMap){ //++++
        logger.debug("Received request to update user");

        String salt = new String(Hex.encode(simpleUser.getUsername().getBytes())); //++++
        simpleUser.setSalt(salt);                                              //++++
        simpleUser.setPassword(passwordEncoder.encodePassword(simpleUser.getPassword(), salt));   //++++
        simpleUser.setEnabled(true);                     //++++
        User user = userService.update(id, simpleUser);      //++++
        modelMap.addAttribute("user", user);
        modelMap.addAttribute("menuUserClass", "active");

        return "user-data-tiles";
    }

    @RequestMapping(method = RequestMethod.GET)
    public String findAll(ModelMap modelMap){
        logger.debug("Received request to get list user");

        List<User> userList = userService.findByRole(roleService.findByAuthority("ROLE_STUDENT"));
//        List<User> userList = userService.findAll();
        modelMap.addAttribute("userList", userList);
        modelMap.addAttribute("menuUserClass", "active");

        return "user-list-tiles";
    }

    @RequestMapping(params = {"id"}, method = RequestMethod.GET)
    public String findById(@RequestParam("id")Integer id, ModelMap modelMap){
        logger.debug("Received request to get data user");

        User user = userService.findById(id);
        modelMap.addAttribute("user", user);
        modelMap.addAttribute("menuUserClass", "active");

        return "user-data-tiles";
    }

    @RequestMapping(value = "/delete", params = {"id"}, method = RequestMethod.GET)
    public String delete(@RequestParam("id")Integer id, ModelMap modelMap){
        logger.debug("Received request to delete user");

        userService.delete(id);

        return "redirect:/user";
    }

}
