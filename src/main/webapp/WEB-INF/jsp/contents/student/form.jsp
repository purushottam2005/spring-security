<%--
  Created by IntelliJ IDEA.
  User: LATIEF-NEW
  Date: 4/19/13
  Time: 10:25 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<div class="alert" style="background-color: #ffffff;">

    <div class="alert alert-info">
        <h4>Insert Student Data</h4>
    </div>

    <form:form modelAttribute="student" method="POST" action="${studentUrl}" class="form-horizontal">

        <div class="control-group">
            <form:label class="control-label" path="firstName">First Name</form:label>
            <div class="controls">
                <form:input type="text" path="firstName"></form:input>
            </div>
        </div>
        <div class="control-group">
            <form:label class="control-label" path="lastName">Last Name</form:label>
            <div class="controls">
                <form:input type="text" path="lastName"></form:input>
            </div>
        </div>
        <div class="control-group">
            <form:label class="control-label" path="address">Address</form:label>
            <div class="controls">
                <form:input type="text" path="address"></form:input>
            </div>
        </div>

        <div class="control-group">
            <form:label class="control-label" path="sex">Sex</form:label>
            <div class="controls">
                <form:select type="text" path="sex">
                    <%--<form:options items="${sexs}"></form:options>--%>
                    <form:option value="MALE">MALE</form:option>
                    <form:option value="FEMALE">FEMALE</form:option>
                    <form:option value="TRANSGENDER">TRANSGENDER</form:option>
                </form:select>
            </div>
        </div>
        <div class="control-group">
            <form:label class="control-label" path="age">Age</form:label>
            <div class="controls">
                <form:input type="text" path="age"></form:input>
            </div>
        </div>

        <input class="btn" type="reset" value="Reset"/>
        <input class="btn btn-primary" type="submit" value="Submit"/>
    </form:form>
</div>
