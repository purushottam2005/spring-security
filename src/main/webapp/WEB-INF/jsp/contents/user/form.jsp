<%--
  Created by IntelliJ IDEA.
  User: LATIEF-NEW
  Date: 4/19/13
  Time: 10:25 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<div class="alert" style="background-color: #ffffff;">

    <div class="alert alert-info">
        <h4>Insert User Data</h4>
    </div>

    <form:form modelAttribute="user" method="POST" action="${userUrl}" class="form-horizontal">

        <div class="control-group">
            <form:label class="control-label" path="username">Username</form:label>
            <div class="controls">
                <form:input type="text" path="username"></form:input>
            </div>
        </div>
        <div class="control-group">
            <form:label class="control-label" path="password">Password</form:label>
            <div class="controls">
                <form:password path="password"></form:password>
            </div>
        </div>
        <%--role checkbock--%>
        <div class="control-group">
            <label class="control-label" path="authorities">Role</label>&nbsp;&nbsp;
            <c:forEach items="${roleList}" var="role" varStatus="status">
                <label class="checkbox inline">
                    <form:checkbox id="${role.authority}1" value="${role.authority}" path="authorities" /> ${role.authority}
                </label>
            </c:forEach>
        </div>
        <%--role checkbock--%>

        <input class="btn" type="reset" value="Reset"/>
        <input class="btn btn-primary" type="submit" value="Submit"/>
    </form:form>
</div>
