<%--
  Created by IntelliJ IDEA.
  User: LATIEF-NEW
  Date: 4/19/13
  Time: 10:25 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div class="alert" style="background-color: #ffffff;">
    <div class="alert alert-info">
        <h4>
            Role List
            <a href="<c:url value='/role/form'/>">
                <button class="btn btn-small">
                    <i class="icon-plus"></i>
                </button>
            </a>
        </h4>
    </div>
    <table class="table table-striped">
        <thead>
        <td>Authority</td>
        <td>Action</td>
        </thead>
        <tbody>

        <c:forEach items="${roleList}" var="role">
            <tr>
                <td><a href="<c:url value='/role?authority=${role.authority}'/>">${role.authority}</a></td>
                <td>
                    <a href="<c:url value='/role/delete?authority=${role.authority}'/>">
                        <button class="btn btn-primary btn-danger">
                            <i class="icon-white icon-remove"></i>
                        </button>
                    </a>
                </td>
            </tr>
        </c:forEach>

        </tbody>
    </table>
</div>