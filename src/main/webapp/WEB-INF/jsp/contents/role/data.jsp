<%--
  Created by IntelliJ IDEA.
  User: LATIEF-NEW
  Date: 4/19/13
  Time: 10:25 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<div class="alert" style="background-color: #ffffff;">

    <div class="alert alert-info">
        <h4>This Role Data</h4>
    </div>
    <table>
        <tr>
            <td>Authority :</td>
            <td>${role.authority}</td>
        </tr>
    </table>
    <br>
    <a href="<c:url value='/role'/>"><button class="btn">Back</button></a>
</div>
