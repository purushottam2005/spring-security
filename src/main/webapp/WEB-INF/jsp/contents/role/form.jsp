<%--
  Created by IntelliJ IDEA.
  User: LATIEF-NEW
  Date: 4/19/13
  Time: 10:25 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<div class="alert" style="background-color: #ffffff;">

    <div class="alert alert-info">
        <h4>Insert Role Data</h4>
    </div>

    <form:form modelAttribute="role" method="POST" action="${roleUrl}" class="form-horizontal">

        <div class="control-group">
            <form:label class="control-label" path="authority">Authority</form:label>
            <div class="controls">
                <form:input type="text" path="authority"></form:input>
            </div>
        </div>

        <input class="btn" type="reset" value="Reset"/>
        <input class="btn btn-primary" type="submit" value="Submit"/>
    </form:form>
</div>
