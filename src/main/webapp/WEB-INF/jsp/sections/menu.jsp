<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<div class="navbar">
    <div class="navbar-inner">
        <div class="container">
            <ul class="nav">
                <li class="${menuHomeClass}"><a href="<c:url value='/'/>">Home</a></li>
                <sec:authorize access="hasAnyRole('ROLE_ADMIN','ROLE_STUDENT')">
                    <li class="${menuStudentClass }"><a href="<c:url value='/student'/>">Student</a></li>
                </sec:authorize>
                <sec:authorize access="hasAnyRole('ROLE_ADMIN','ROLE_TEACHER')">
                    <li class="${menuTeacherClass }"><a href="<c:url value='/teacher'/>">Teacher</a></li>
                </sec:authorize>
                <sec:authorize access="hasAnyRole('ROLE_ADMIN','ROLE_SUBJECT')">
                    <li class="${menuSubjectClass }"><a href="<c:url value='/subject'/>">Subject</a></li>
                </sec:authorize>
                <sec:authorize access="hasAnyRole('ROLE_ADMIN','ROLE_LECTURE')">
                    <li class="${menuLectureClass }"><a href="<c:url value='/lecture'/>">Lecture</a></li>
                </sec:authorize>
                <sec:authorize access="hasAnyRole('ROLE_ADMIN','ROLE_USER')">
                    <li class="${menuUserClass }"><a href="<c:url value='/user'/>">User</a></li>
                </sec:authorize>
                <sec:authorize access="hasAnyRole('ROLE_ADMIN','ROLE_ROLE')">
                    <li class="${menuRoleClass }"><a href="<c:url value='/role'/>">Role</a></li>
                </sec:authorize>
            </ul>
        </div>
    </div>
</div>